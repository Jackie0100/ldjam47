﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverCell : LivingCells
{
    public override void Step()
    {
        if (!IsMoving)
        {
            StartCoroutine(IterateStep());
        }
    }

    public override void StepBack()
    {
        if (!IsMoving)
        {
            StartCoroutine(IterateStepBack());
        }
    }

    IEnumerator IterateStep()
    {
        IsMoving = true;
        float timer = 0;
        Vector2 curpos = Location;
        var targetpos = transform.TransformVector(Vector2.right / transform.localScale.x);
        while (timer < 1)
        {

            timer += Time.deltaTime / MoveTime;
            transform.position = curpos + (Vector2)(targetpos * timer);
            yield return new WaitForEndOfFrame();
        }

        Location = RoundLocation(transform.position);
        transform.position = new Vector3(Location.x, Location.y, 0);
        IsMoving = false;
    }

    IEnumerator IterateStepBack()
    {
        IsMoving = true;
        float timer = 0;
        Vector2 curpos = Location;
        var targetpos = transform.TransformVector(Vector2.left / transform.localScale.x);
        while (timer < 1)
        {

            timer += Time.deltaTime / MoveTime;
            transform.position = curpos + (Vector2)(targetpos * timer);
            yield return new WaitForEndOfFrame();
        }

        Location = RoundLocation(transform.position);
        transform.position = new Vector3(Location.x, Location.y, 0);
        IsMoving = false;
    }
}
