﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Grid : MonoBehaviour
{
    public Cell[,] cells;
    public List<LivingCells> livingcells;

    [SerializeField]
    Vector2Int _gridSize;
    [SerializeField]
    float _scale = 3.125f;
    [SerializeField]
    GameObject _cellToGenerate;
    [SerializeField]
    GameObject _lockedCellToGenerate;
    [SerializeField]
    GameObject _wallCellsToGenerate;

    [SerializeField]
    Vector2Int _gridNonLockedCellSize;

    public static Grid Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        var allcells = FindObjectsOfType<Cell>();

        Debug.Log(allcells.Max(t => t.Location.x));
        Debug.Log(allcells.Max(t => t.Location.y));

        cells = new Cell[allcells.Max(t => t.Location.x) + 1, allcells.Max(t => t.Location.y) + 1];
        livingcells = new List<LivingCells>();

        foreach (var cell in allcells)
        {
            Debug.Log(cell.Location);
            cells[cell.Location.x, cell.Location.y] = cell;
        }

        var alllivingcells = FindObjectsOfType<LivingCells>();
        livingcells.AddRange(alllivingcells);
    }

    [Button]
    private void GenerateGrid()
    {
        for (int x = 0; x < _gridSize.x; x++)
        {
            for (int y = 0; y < _gridSize.y; y++)
            {
                if (x == 0 || x == _gridSize.x - 1 || y == 0 || y == _gridSize.y - 1)
                {
                    Instantiate(_wallCellsToGenerate, new Vector3(x, y, 0), Quaternion.identity, transform);
                }
                else
                {
                    Instantiate(_lockedCellToGenerate, new Vector3(x, y, 0), Quaternion.identity, transform);
                }
            }
        }
    }

    [Button]
    private void GenerateNonLockCells()
    {

    }
}
