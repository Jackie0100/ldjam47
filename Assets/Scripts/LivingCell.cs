﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LivingCells : MonoBehaviour
{
    [SerializeField]
    private int _executionOrder;
    [SerializeField]
    bool _isMoving;
    [SerializeField]
    float _moveTime;

    public Vector2Int Location { get; protected set; }

    public int ExecutionOrder
    {
        get
        {
            return _executionOrder;
        }
        set
        {
            _executionOrder = value;
        }
    }

    public bool IsMoving
    {
        get
        {
            return _isMoving;
        }
        protected set
        {
            _isMoving = value;
        }
    }

    protected float MoveTime
    {
        get
        {
            return _moveTime;
        }

        set
        {
            _moveTime = value;
        }
    }

    protected virtual void Awake()
    {
        Location = RoundLocation(transform.position);
    }

    [Button]
    public abstract void Step();
    [Button]
    public abstract void StepBack();

    public virtual Vector2Int RoundLocation(Vector2 vec)
    {
        return new Vector2Int(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.y));
    }
}
