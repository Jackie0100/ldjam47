﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Vector3GameEventListener : GameEventListener<Vector3>
{
    [SerializeField] private Vector3GameEvent _gameEvent;
    [SerializeField] private Vector3UnityEvent _response;

    public override GameEvent<Vector3> GameEvent
    {
        get
        {
            return _gameEvent;
        }
    }

    public override UnityEvent<Vector3> Response
    {
        get
        {
            return _response;
        }
    }
}
