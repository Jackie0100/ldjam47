﻿using UnityEngine;

[CreateAssetMenu(menuName = "Events/Vector2GameEvent")]
public class Vector2GameEvent : GameEvent<Vector2>
{
}