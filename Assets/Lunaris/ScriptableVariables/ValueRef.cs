﻿using UnityEngine;

namespace Lunaris.ScriptableVariables
{
    public abstract class ValueRef<T> : ScriptableObject
    {
        [SerializeField]
        protected T value;

        public virtual T Value
        {
            get
            {
                return value;
            }

            set
            {
                this.value = value;
            }
        }
    }
}
