﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lunaris.ScriptableVariables
{
    public class ObservableCollectionRef<T> : ScriptableObject, IEnumerable
    {
        [SerializeField]
        protected List<T> _value;
        [SerializeField]
        GameEvent _onValueChangedEvent;
        public event OnCollectionChanged OnValueChanged;

        public void Reset()
        {
            _value = new List<T>();
        }

        public T this[int index]
        {
            get
            {
                return _value[index];
            }

            set
            {
                _value[index] = value;
                OnValueChanged?.Invoke();
                _onValueChangedEvent?.Invoke();
            }
        }

        public int Count
        {
            get
            {
                return _value.Count;
            }
        }

        public void Add(T value)
        {
            _value.Add(value);
            OnValueChanged?.Invoke();
            _onValueChangedEvent?.Invoke();
        }

        public void Clear()
        {
            _value.Clear();
            OnValueChanged?.Invoke();
            _onValueChangedEvent?.Invoke();
        }

        public bool Contains(T value)
        {
            return _value.Contains(value);
        }

        public void CopyTo(T[] array, int index)
        {
            _value.CopyTo(array, index);
        }

        public IEnumerator GetEnumerator()
        {
            return _value.GetEnumerator();
        }

        public int IndexOf(T value)
        {
            return _value.IndexOf(value);
        }

        public void Insert(int index, T value)
        {
            _value.Insert(index, value);
        }

        public void Remove(T value)
        {
            _value.Remove(value);
            OnValueChanged?.Invoke();
            _onValueChangedEvent?.Invoke();
        }

        public void RemoveAt(int index)
        {
            _value.RemoveAt(index);
            OnValueChanged?.Invoke();
            _onValueChangedEvent?.Invoke();
        }
    }

    public delegate void OnCollectionChanged();
}